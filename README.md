# Fullstack - Todos

## React + NodeJS API to test UpCloud DBaaS

To start react app

```
cd app
npm start
```

To NodeJS API

```
cd server
node index.js
```

To access your db create .env file and fill your credentials.
