import React from "react";
import AddTodo from "./components/AddTodo";
import ListTodos from "./components/ListTodos";

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <AddTodo />
          <ListTodos />
        </div>
      </div>
    </div>
  );
}

export default App;
