import React, { useState } from "react";

const AddTodo = () => {
  const [description, setDescription] = useState("");

  const onFormSubmit = async (e) => {
    //prevent reload on form submit
    e.preventDefault();
    try {
      const body = { description };
      // fetch is always a promise
      const response = await fetch("http://localhost:5000/todos", {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(body),
      });

      if (response.ok)
        //refresh
        window.location.reload();
    } catch (error) {
      console.error(error.message);
    }
  };

  return (
    <>
      <h2 className="mt-5 mb-4">Todos</h2>
      <form onSubmit={onFormSubmit}>
        <div className="input-group mb-5">
          <input
            type="text"
            className="form-control"
            placeholder="Add todo"
            aria-label="Recipient's username"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          <div className="input-group-append">
            <button className="btn btn-outline-secondary">Add</button>
          </div>
        </div>
      </form>
    </>
  );
};

export default AddTodo;
