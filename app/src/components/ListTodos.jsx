import React, { useState, useEffect } from "react";

import { EditTodo } from "./EditTodo";

const ListTodos = () => {
  const [todos, setTodos] = useState([]);

  const getTodos = async () => {
    try {
      //default fetch is GET
      const response = await fetch("http://localhost:5000/todos");
      const data = await response.json();

      setTodos(data);
    } catch (err) {
      console.error(err.message);
    }
  };

  const deleteTodo = async (id) => {
    try {
      const response = await fetch(`http://localhost:5000/todos/${id}`, {
        method: "DELETE",
      });

      if (response.ok)
        //remove deleted todo from current list
        setTodos(todos.filter((todo) => todo.todo_id !== id));
    } catch (err) {
      console.error(err.message);
    }
  };

  //fetch todos on comp load
  useEffect(() => {
    getTodos();
  }, []);

  return (
    <>
      <table className="table table-borderless">
        <thead>
          <tr>
            <th colSpan="5">Description</th>
            <th colSpan="1">Edit</th>
            <th colSpan="1">Delete</th>
          </tr>
        </thead>
        <tbody>
          {todos.map((todo, index) => (
            <tr key={index}>
              <td colSpan="5">{todo.description}</td>
              <td colSpan="1">
                <EditTodo todo={todo} />
              </td>
              <td colSpan="1">
                <button
                  type="button"
                  className="btn btn-danger btn-sm"
                  onClick={() => deleteTodo(todo.todo_id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default ListTodos;
