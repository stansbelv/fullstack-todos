const Pool = require("pg").Pool;
const fs = require("fs");
require("dotenv").config();

const pool = new Pool({
	user: process.env.DB_USER,
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	database: process.env.DB_NAME,
	password: process.env.DB_PASS,
	ssl: {
		rejectUnauthorized: false,
		ca: fs.readFileSync("ca.cer").toString(),
	},
});

module.exports = pool;
